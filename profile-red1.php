<?php include('header_all.php') ?>
<header style="background: #EB5757" id="products">
        <div class="title-lg">
            <h3>Profile</h3>
            <span>YukDaeJang</span>
        </div>
</header>
<div class="wrapper line-top">
<div id="profile-red1" class="profile" >
    <section class="profile-fr">
        <div class="container-fluid">
            <form action="">
                <div class="form-prfile">
                    <div class="title">
                        <div class="icon">
                            <a href="#">
                                <img src="images/Vector.svg" alt="">
                            </a>
                        </div>
                        <div class="desc">
                            <b>YukDaeJang</b>
                            <p>YukDaeJang1210@gmail.com</p>
                        </div>
                    </div>
                    <div class="content">
                        <div class="form-group">
                            <label for="pwd">Name:</label>
                            <input type="text" class="form-control" placeholder="YukDaeJang">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Email:</label>
                            <input type="email" class="form-control" placeholder="YukDaeJang1210@gmail.com">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Phone number:</label>
                            <input type="number" class="form-control" placeholder="+84 4302424332">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Address:</label>
                            <input type="text" class="form-control" placeholder="Number 2, Ton Duc Thang, Ben Nghe Ward.">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Address:</label>
                            <input type="text" class="form-control" placeholder="Number 2, Ton Duc Thang, Ben Nghe Ward.">
                        </div>
                    </div>
                </div>
                <div class="btn-login btn-profile">
                    <button class="btn btn-danger">Log out</button>
                </div>
            </form>
        </div>
    </section>
</div>


<?php include('footer_all.php') ?>