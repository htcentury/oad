<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="CSS/library/bootstrap.min.css">
    <link rel="stylesheet" href="CSS/library/fontawesome.min.css">
    <script src="Assets/library/bootstrap.min.js"></script>
    <script src="Assets/library/fontawesome.min.js"></script>
    <title>ODA</title>
</head>
<body>
<div class="wrapper-home">