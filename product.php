<?php include('header_all.php') ?>
<header style="background: #EB5757" id="products">
        <div class="title-lg">
            <h3>Products</h3>
            <span>YukDaeJang</span>
        </div>
</header>
<div class="wrapper">
<div id="product">
<section id="prod-sup">
    <div class="container-fluid">
       <div class="list">
        <div class="prod-chec">
                <div class="icheck-material-pink">
                        <input type="checkbox" id="SupplierA" value="" class="order-checked">
                        <label for="SupplierA"></label>
                        <span>Supplier A</span>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item1" value="" class="order-checked">
                        <label for="item1" ></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item2" value="" class="order-checked">
                        <label for="item2"></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
       </div>
       <div class="list">
        <div class="prod-chec">
                <div class="icheck-material-pink">
                        <input type="checkbox" id="item3" value="" class="order-checked">
                        <label for="item3"></label>
                        <span>Supplier B</span>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item4" value="" class="order-checked">
                        <label for="item4"></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item5" value="" class="order-checked">
                        <label for="item5"></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
       </div>
       <div class="list">
        <div class="prod-chec">
            <div class="icheck-material-pink">
                        <input type="checkbox" id="item5" value="" class="order-checked">
                        <label for="item5"></label>
                        <span>Supplier C</span>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item6" value="" class="order-checked">
                        <label for="item6"></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
            <div class="item">
                <div class="pro-child">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="item7" value="" class="order-checked">
                        <label for="item7"></label>
                    </div>
                    <div class="img-pr">
                        <a href="#"><img src="images/img-prod.png" alt=""></a>
                    </div>
                    <div class="pro-title">
                        <div class="ct-prod">
                            <a class="h3-xam">Product A</a>
                            <p>(35,000 / kg)</p>
                        </div>
                        <div class="quantity">
                            <input type="number" min="1" max="9" step="1" value="1">
                        </div>
                    </div>
                </div>
                <div class="price">
                        <p>70,000 VND</p>
                </div>
            </div>
       </div>
    </div>
    <div id="pro-order">
        <div class="container-fluid">
            <div class="icheck-material-pink">
                        <input type="checkbox" id="item8" value="">
                        <label for="item8"></label>
                    </div>
            <p>Choose all</p>
            <div id="total-price">
                <p>Total price</p>
                <p>280,000 VND</p>
            </div>
            <button class="btn btn-danger">Order</button>
        </div>
    </div>
</section>
</div>


<?php include('footer_all.php') ?>