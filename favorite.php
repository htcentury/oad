<?php include('header_all.php') ?>
<header style="background: #EB5757" id="products">
        
        <div class="title-lg">
            <div class="arr-back">
                <a href="#"><img src="images\Shape-left.png"></a>
            </div>
            <h3>Order 182</h3>
            <span>YukDaeJang</span>
        </div>
</header>
<div class="wrapper space-padding line-top">
    <div id="favorite" class="line-child">
        <section id="fav-fa">
            <div class="container-fluid">
                <div class="fav-sup-info line-title" >
                    <div class="header-fav-info">Supplier info</div>
                    <div class="conta-fav-info">
                        <p>Name: SkypeMark</p>
                        <p>Phone number: +84 43432</p>
                    </div>
                    <div class="date-fav-info">
                        <p>Order date:</p>
                        <p>2019-11-20</p>
                    </div>
                </div>
        </section>
        <section id="fav-sup-list">
            <div class="list-item">
                <div class="header-item">
                    Product
                </div>
                <div class="container-fluid">
                    <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                 <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                           
                            <div class="item-status">
                             <p class="confirm">Confirmed</p>
                            </div>
                        </div>
                    </div>

                     <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                 <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                           
                            <div class="item-status">
                             <p class="cancelled">Cancelled</p>
                            </div>
                        </div>
                    </div>

                     <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                 <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                           
                            <div class="item-status">
                             <p class="pospone">Postpone <br> 2019 - 11- 30</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
</div>


<?php include('footer_all.php') ?>