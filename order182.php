<?php include('header_all.php') ?>
<header class="head_blue" id="products">
        <div class="title-lg">
            <div class="arr-back">
                <a href="#"><img src="images\Shape-left.png"></a>
            </div>
            <h3>Order 182</h3>
            <span>YukDaeJang</span>
        </div>
</header>
<div class="wrapper space-padding line-top">
    <div id="order-green" class="line-child">
        <section id="fav-sup">
            <div class="container-fluid">
                <div class="fav-sup-info line-title" >
                    <div class="header-fav-info">Buyer info</div>
                    <div class="conta-fav-info">
                        <p>Name: SkypeMark</p>
                        <p>Phone number: +84 43432</p>
                        <p>Address : ...</p>
                    </div>
                    <div class="date-fav-info">
                        <p>Order date:</p>
                        <p>2019-11-20</p>
                    </div>
                    <div class="price-fav-info">
                        <p>Total price :</p>
                        <p>5,000,000 VND</p>
                    </div>
                </div>
        </section>
        <section id="fav-sup-list">
            <div class="list-item">

                <div class="header-item">
                    <div class="icheck-material-pink">
                        <input type="checkbox" id="itemall" value="" class="order-checked">
                        <label for="item"> </label>
                    </div>
                    <span>Product</span>
                   
                </div>
                <div class="container-fluid">
                    <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                <div class="icheck-material-pink">
                                    <input type="checkbox" id="item4" value="" class="order-checked">
                                    <label for="item4"></label>
                                </div>
                                 <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                <div class="icheck-material-pink">
                                    <input type="checkbox" id="item4" value="" class="order-checked">
                                    <label for="item4"></label>
                                </div>
                                <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="fav-item">
                        <div class="fav-item-info">
                            <div class="wrap-item-info">
                                <div class="icheck-material-pink">
                                    <input type="checkbox" id="item4" value="" class="order-checked">
                                    <label for="item4"></label>
                                </div>
                                 <div class="img-pr">
                                <a href="#"><img src="images/img-prod.png" alt=""></a>
                                </div>
                                <div class="fav-item-detail">
                                    <div class="fav-detail">
                                        <p>Product A</p>
                                        <p>(35,000 / kg) x 3</p>
                                    </div>
                                    <div class="fav-price">
                                        <p>105,000 VND</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
</div>

<footer>
    <div class="container-fluid">
        <div class="icon-bottom">
            <div class="item">
                <a href="#"><img src="images/box.png" alt=""></a>
                <p>Out of stock</p>
            </div>
           <div class="item">
                <a href="#"><img src="images/left-right.png" alt=""></a>
                <p>Change</p>
            </div>
            <div class="item">
                <a href="#"><img src="images/load.png" alt=""></a>
                <p>Postpone</p>
            </div>
            <div class="item">
                <a href="#"><img src="images/checkbox.png" alt=""></a>
                <p>Confirm</p>
            </div>
        </ul>
    </div>
    <div class="line-bar"></div>
</footer>
    
</div>

<script src="js/main.js"></script>
</body>
</html>