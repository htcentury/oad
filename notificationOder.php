<?php include('header_all.php') ?>
<header class="head_blue" id="products">
        <div class="title-lg">
            <h3>Notification</h3>
            <span>SkyMart</span>
        </div>
</header>
<div class="wrapper space-padding line-top">
	<div id="notifi-red" class="line-child">
		<section id="notifi-sup">
			<div class="noti-box noti-confirm">
				<div class="container-fluid">
					<div class="noti-text">
						The order 182 has been confirmed
					</div>
					<div class="noti-time">
						2019.11.20 15:00
					</div>
				</div>
			</div>
			<div class="noti-box">
				<div class="container-fluid">
					<div class="noti-text">
						The order 182 has been confirmed
					</div>
					<div class="noti-time">
						2019.11.20 15:00
					</div>
				</div>
			</div>
		</section>
	</div>
<?php include('footer_all.php') ?>