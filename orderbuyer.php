<?php include('header_all.php') ?>
<header style="background: #EB5757" id="products">
        <div class="title-lg">
            <h3>Order</h3>
            <span>YukDaeJang</span>
        </div>
</header>
<div class="wrapper line-top">
<div id="order-buyer" class="line-child">
<section id="buyer-sup">
   <div class="order-wrap-header line-title">
   		<div class="sub-title">
   			<h5>Pending</h5>
   		</div>
   	</div>
   	<div class="container-fluid order-wrap-contain">
   		<div class="order-table">
   			<table style="width:100%">
			  <tr>
			    <th>Order No</th>
			    <th>Supplier</th>
			    <th>No, Prods</th>
			    <th>Amount</th>
			    <th>Date</th>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			</table>
   		</div>
   </div>

	<div class="order-wrap-header">
   		<div class="sub-title">
   			<h5>Confirmed</h5>
   		</div>
   	</div>
   	<div class="container-fluid order-wrap-contain">
   		<div class="order-table">
   			<table style="width:100%">
			  <tr>
			    <th>Order No</th>
			    <th>Supplier</th>
			    <th>No, Prods</th>
			    <th>Amount</th>
			    <th>Date</th>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			  <tr>
			    <td>182</td>
			    <td>SkyMart</td>
			    <td>3</td>
			    <td>1,166,000</td>
			    <td>2019.11.20 <br> 15:00</td>
			  </tr>
			</table>
   		</div>
   </div>
</section>
</div>

<?php include('footer_all.php') ?>