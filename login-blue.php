<?php include('header_all.php') ?>
<header class="head_lg head_blue" id="login-supl">
   <div class="title-lg">
      <h3>Supplier login</h3>
   </div>
</header>

<div class="wrapper">
<div id="Suppl-login" >
<section class="buyer-login" >
   <div class="container-fluid">
      <form action="">
         <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="email" type="email" class="form-control" name="email" placeholder="Email ID">
         </div>
         <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
         </div>
         <div class="forgot">
            <a href="#">Forgot Password?</a>
         </div>
         <div class="btn-login">
            <button class="btn btn-danger">Login</button>
         </div>
      </form>
   </div>
</section>
</div>
</div>
</div>

<script src="js/main.js"></script>
</body>
</html>